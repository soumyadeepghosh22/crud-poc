export function getAllEmployees(){
    const employees = JSON.parse(localStorage.getItem('employees')) || [];
    return employees;
}

export function getEmployeeById(id){
    if(!id) return {};

    const employees = getAllEmployees();
    let employee = employees.filter(emp => emp.id === id)[0];
    return employee || {};
}

export function addEmployee(data){
    const employees = getAllEmployees();
    data.id = employees.length + 1;
    employees.push(data);
    localStorage.setItem('employees', JSON.stringify(employees));
    return true;
}

export function updateEmployee(data, id){
    const employee = getEmployeeById(id);
    if(employee){
        const employees = getAllEmployees();
        data.id = employee.id;
        employees.map(emp => {
            if(emp.id === id){
                emp.firstName = data.firstName;
                emp.lastName = data.lastName;
                emp.email = data.email;
                if(data.mobileNo)
                    emp.mobileNo = data.mobileNo;
                emp.sex = data.sex;
                emp.role = data.role;
                emp.doj = data.doj;
            }
            return emp;
        });
        localStorage.setItem('employees', JSON.stringify(employees));
    }
}

export function deleteEmployee(id){
    const employee = getEmployeeById(id);
    if(employee){
        let employees = getAllEmployees();
        employees = employees.filter(emp => emp.id !== id);
        console.warn(employees)
        localStorage.setItem('employees', JSON.stringify(employees));
    }
}