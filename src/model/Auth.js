export function login(data){

    if(data.email !== "admin@example.com" || data.password !== "Password123"){
        return false;
    }

    localStorage.setItem("user", "admin");
    return true;
}

export function hasAuth(){
    const user = localStorage.getItem("user");
    return user ? true : false;
}

export function logout(){
    localStorage.removeItem("user");
}