import React, { useState } from 'react'
import InputBox from '../common/InputBox'
import { login } from '../../model/Auth'
import { useNavigate } from 'react-router-dom';

export default function Login({ triggerChange }) {
  const [data, setData] = useState({});

  const navigate = useNavigate();

  function handleInputChange(event) {
    const target = event.currentTarget

    setData({
      ...data,
      [target.name]: target.value,
    })
  }

  function handleLogin(event) {
    event.preventDefault();

    if (login(data)) {
      triggerChange();
      navigate("/dashboard");
    } else {
      window.alert('Login failed');
    }
  }

  return (
    <div class="d-flex justify-content-center">
      <div class="card" style={{ width: "300px" }}>
        <div class="card-header">
          <h4 className="text-center">Login</h4>
        </div>
        <div class="card-body">
          <form onSubmit={handleLogin}>
            <div className="mb-3">
              <InputBox label="Email" type="email" name="email" placeholder="Enter your email" isRequired={true} onInputChange={handleInputChange} />
            </div>
            <div className="mb-3">
              <InputBox label="Password" type="password" name="password" placeholder="Enter your password" isRequired={true} onInputChange={handleInputChange} />
            </div>
            <div className="d-flex justify-content-center">
              <button type="submit" className="btn btn-primary">Login</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}
