import React,{useState, useEffect} from 'react';
import { getAllEmployees, deleteEmployee } from '../../model/Employees';

export default function EmployeeList(props) {
  const [data, setData] = useState([]);

  function loadEmployeesData(){
    setData(getAllEmployees());
  }

  useEffect(() => loadEmployeesData(), []);

  function requestUpdate(id){
    props.getId(id);
    props.triggerDisplay(true);
    loadEmployeesData();
  }

  function requestDelete(id){
    let popup = window.confirm('Are you sure you want to delete?');
    if(popup){
      deleteEmployee(id);
      loadEmployeesData();
    }
  }

  return (
    <>
      <table className="table table-bordered table-striped table-hover">
        <thead>
          <tr className="bg-dark text-white text-center">
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Mobile No.</th>
            <th>Role</th>
            <th>Sex</th>
            <th>DOJ</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {
            (data && data.length > 0) ? data.map(({id, firstName, lastName, email, mobileNo, role, sex, doj}) => {
              return (
                <tr>
                  <td>{id}</td>
                  <td>{firstName + " " + lastName}</td>
                  <td>{email}</td>
                  <td>{mobileNo || "N/A"}</td>
                  <td>{role}</td>
                  <td>{sex}</td>
                  <td>{doj}</td>
                  <td className="text-center">
                    <button type="button" className="btn btn-sm btn-info mx-2" onClick={() => requestUpdate(id)}>
                      <i className="fa fa-pencil"></i>
                    </button>
                    <button type="button" className="btn btn-sm btn-danger" onClick={() => requestDelete(id)}>
                      <i className="fa fa-trash"></i>
                    </button>
                  </td>
                </tr>
              );
            }) : (
              <tr className="text-center">
                <td colSpan="8">No record found</td>
              </tr>
            )
          }
        </tbody>
      </table>
    </>
  )
}
