import React from 'react'

export default function Home() {
  return (
    <div className="d-flex justify-content-center fs-1">
      <h1>Employee Management System</h1>
    </div>
  )
}
