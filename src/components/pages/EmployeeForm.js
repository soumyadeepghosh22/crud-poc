import React,{useEffect, useState} from 'react';
import { addEmployee, getEmployeeById, updateEmployee } from '../../model/Employees';
import InputBox from '../common/InputBox';

export default function EmployeeForm(props) {
  const roleOptions = [{'label': 'Developer', 'value': 'developer'}, {'label': 'Tester', 'value': 'tester'}];
  const sexOptions = [{'label': 'Male', 'value': 'male'}, {'label': 'Female', 'value': 'female'}, {'label': 'Other', 'value': 'other'}];

  const [data, setData] = useState({});

  function fetchData(){
    if(props.empId){
      console.warn(getEmployeeById(props.empId));
      setData(getEmployeeById(props.empId));
    }else{
      setData({});
    }
  }

  useEffect(() => fetchData(), [props.empId]);

  function handleInputChange(event){
    const target = event.currentTarget

    setData({
      ...data,
      [target.name]: (target.type === 'checkbox') ? target.checked : target.value,
    })
  }

  function closePanel(){
    props.triggerDisplay(false);
    setData({});
  }

  function handleFormSubmit(event){
    event.preventDefault();
    if(!props.empId){
      addEmployee(data);
    }else{
      updateEmployee(data, props.empId);
    }
    closePanel();
  }

  return (
    <div className="m-3 p-3">
      <h2>Employee Form</h2>
      <form onSubmit={handleFormSubmit}>
        <div className="row mb-2">
          <div className="col-md-6">
            <InputBox label="First Name" value={data.firstName} name= "firstName" type="text" onInputChange ={handleInputChange} placeholder="Enter First Name" isRequired ={true} />
          </div>
          <div className="col-md-6">
            <InputBox label="Last Name" value={data.firstName} name= "lastName" type="text" onInputChange ={handleInputChange} placeholder="Enter Last Name" isRequired ={true} />
          </div>
        </div>
        <div className="row mb-2">
          <div className="col-md-12">
            <label className="form-label fw-bold required">Sex</label>
            {
              sexOptions.map((sex) => {
                return (
                  <div className="form-check">
                    <input type="radio" className="form-check-input" name="sex" key={sex.value} value={sex.value} checked={sex.value === data.sex} onChange={handleInputChange} required />
                    <label className="form-check-label">{sex.label}</label>
                  </div>
                )
              })
            }
          </div>
        </div>
        <div className="row mb-2">
          <div className="col-md-6">
            <InputBox label="Email" value={data.firstName} name="email" type="email" onInputChange={handleInputChange} placeholder="Enter Email" isRequired={true} />
          </div>
          <div className="col-md-6">
            <InputBox label="Mobile Number" value={data.firstName} name="mobNo" type="number" onInputChange={handleInputChange} placeholder="Enter Mobile Number" />
          </div>
        </div>
        <div className="row mb-2">
          <div className="col-md-6">
            <label className="form-label fw-bold required">Role</label>
            <select className="form-control" name="role" onChange={handleInputChange} value={data.role}>
              <option value="">--Select--</option>
              {
                roleOptions.map((role) => <option key={role.value} value={role.value}>{role.label}</option>)
              }
            </select>
          </div>
          <div className="col-md-6">
            <InputBox label="Date of Joining" value={data.doj} name="doj" type="date" onInputChange={handleInputChange} />
          </div>
        </div>
        <div className="row mb-2">
          <div className="form-check">
            <input
              type="checkbox"
              className="form-check-input"
              name="tnc"
              onChange={handleInputChange}
              value={data.tnc}
              required
            />
            <label className="form-check-label required">Accept Terms & Condition</label>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-4">
          <button type="button" className="btn btn-default mx-2" onClick={closePanel}>Cancel</button>
          <button type="submit" className="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  )
}
