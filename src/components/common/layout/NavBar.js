import React from 'react'
import { redirect, NavLink } from 'react-router-dom'
import { routes } from '../../routes/routes';
import { hasAuth,logout } from '../../../model/Auth';

export default function NavBar({triggerChange}) {

  function signOut(){
    logout();
    triggerChange();
    return redirect("/login");
  }

  return (
    <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
      <div className="container-fluid">
        <a className="navbar-brand" href="javascript:void(0)">App</a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarText">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {
              routes && routes.map(route => {
                if(route.label === 'Login' && hasAuth()){
                  return(
                    <li className="nav-item">
                      <a href="javascript:void(0)" className="nav-link" onClick={signOut}>Logout</a>
                    </li>
                  )
                }else{
                  return (
                    <li className="nav-item">
                      <NavLink to={route.url} className="nav-link">{route.label}</NavLink>
                    </li>
                  );
                }
              })
            }
          </ul>
          {
            hasAuth() && (
              <span className="navbar-text">
                <i className="fa fa-user-circle"></i> Hello, {localStorage.getItem('user')}
              </span>
            )
          }
        </div>
      </div>
    </nav>
  )
}
