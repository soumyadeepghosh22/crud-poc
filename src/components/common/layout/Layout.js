import React from 'react'
import './css/Layout.css';
import {Outlet} from 'react-router-dom';
import NavBar from './NavBar';
import Footer from './Footer';

export default function Layout({triggerChange}) {
  return (
    <>
      <NavBar triggerChange={() => triggerChange()} />
      <div className="container mx-3 my-3">
        <Outlet />
      </div>
      <Footer />
    </>
  )
}
