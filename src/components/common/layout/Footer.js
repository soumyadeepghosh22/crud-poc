import React from 'react'

export default function Footer() {
  return (
    <footer className="bg-dark text-white">
      &copy; Copyright {new Date().getFullYear()} - All rights reserved
    </footer>
  )
}
