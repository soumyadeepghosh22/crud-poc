import React from 'react'

export default function InputBox({ label, value, placeholder, className, name, type, onInputChange, isRequired }) {
  return (
    <div>
        <label className={"form-label fw-bold"+(isRequired ? " required" : "")}>{label}</label>
        <input
          type={type}
          className={className || "form-control"}
          name={name}
          placeholder={placeholder}
          onChange={onInputChange}
          value={value}
          required={isRequired}
        />
    </div>
  )
}
