import React,{useState, useEffect} from 'react'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from '../common/layout/Layout';
import Home from '../pages/Home';
import Login from '../pages/Login';
import EmployeeList from '../pages/EmployeeList';
import Protected from '../common/Protected';
import {hasAuth} from '../../model/Auth';

export default function AppRouter() {
  const [isSignedIn, setIsSignedIn] = useState(false);

  function checkSignInStatus(){
    if(hasAuth()){
      setIsSignedIn(true);
    }else{
      setIsSignedIn(false);
    }
  }

  useEffect(() => checkSignInStatus(), []);

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout triggerChange={() => checkSignInStatus()} />}>
          <Route index element={<Home />} />
          <Route path="dashboard"  element={
              <Protected isSignedIn={isSignedIn}>
                <EmployeeList />
              </Protected>
            }
          />
          <Route path="login" element={<Login triggerChange={() => checkSignInStatus()} />} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}
