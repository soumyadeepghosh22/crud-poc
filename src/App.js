import {useState} from 'react';
import './App.css';
import EmployeeForm from './components/pages/EmployeeForm';
import EmployeeList from './components/pages/EmployeeList';
import AppRouter from './components/routes/AppRouter';

function App() {
  const [display, setDisplay] = useState(false);
  const [empId, setEmpId] = useState(undefined);

  function openPanel(){
    setDisplay(true);
    setEmpId(undefined);
  }

  return (
    <>
      <AppRouter />
    </>
    // <div className="container mx-3 my-3">
    //   {
    //     display && <EmployeeForm display={display} triggerDisplay={state => setDisplay(state)} empId={empId} />
    //   }

    //   <button type="button" className="btn btn-success my-2" onClick={openPanel} style={{display: (display ? 'none' : '')}}>
    //     <i className="fa fa-plus"></i> Add
    //   </button>

    //   {
    //     !display && <EmployeeList getId={id => setEmpId(id)} triggerDisplay={state => setDisplay(state)} />
    //   }
    // </div>
  );
}

export default App;
